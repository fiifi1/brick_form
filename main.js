const express = require("express");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.set("view engine", "ejs");
app.use(express.static("./public"));

app.get("/", (req, res) => {
    res.render("signUp");
});

app.get("/login", (req, res) => {
    res.render("singIn");
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log("Running"));
